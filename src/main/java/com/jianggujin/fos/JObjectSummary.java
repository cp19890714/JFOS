/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos;

import java.util.Date;

public interface JObjectSummary {
    /**
     * 获得存储空间名称
     * 
     * @return
     */
    String getBucketName();

    /**
     * 对象的实体标签（Entity
     * Tag），是对象被创建时标识对象内容的信息标签，可用于检查对象的内容是否发生变化，例如“8e0b617ca298a564c3331da28dcb50df”，此头部并不一定返回对象的
     * MD5 值，而是根据对象上传和加密方式而有所不同
     * 
     * @return
     */
    String getETag();

    /**
     * 对象键
     * 
     * @return
     */
    String getKey();

    /**
     * 对象最后修改时间
     * 
     * @return
     */
    Date getLastModified();

    /**
     * 对象大小，单位为 Byte
     * 
     * @return
     */
    long getSize();
}
