/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.disk;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.jianggujin.fos.JFOSLazyException;
import com.jianggujin.fos.JObject;
import com.jianggujin.fos.JObjectMetadata;
import com.jianggujin.fos.util.JObjectUtils;

import lombok.Getter;

@Getter
public class JDiskObject implements JObject {
    private String bucketName;
    private String key;
    private File object;
    private volatile JDiskObjectMetadata objectMetadata;
    private volatile InputStream inputStream;

    public JDiskObject(String bucketName, String key, File object) {
        this.bucketName = bucketName;
        this.key = key;
        this.object = object;
    }

    @Override
    public void close() throws IOException {
        JObjectUtils.close(inputStream);
    }

    @Override
    public InputStream getObjectContent() {
        if (inputStream == null) {
            synchronized (this) {
                if (inputStream == null) {
                    try {
                        inputStream = new FileInputStream(object);
                    } catch (IOException e) {
                        throw new JFOSLazyException(e.getMessage(), e);
                    }
                }
            }
        }
        return inputStream;
    }

    @Override
    public JObjectMetadata getObjectMetadata() {
        if (objectMetadata == null) {
            synchronized (this) {
                if (objectMetadata == null) {
                    objectMetadata = new JDiskObjectMetadata(object);
                }
            }
        }
        return objectMetadata;
    }
}