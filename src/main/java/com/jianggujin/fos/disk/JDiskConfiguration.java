/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.disk;

import java.io.File;
import java.util.Collections;
import java.util.Map;

import com.jianggujin.fos.JConfiguration;

import lombok.Data;
import lombok.NonNull;

@Data
public class JDiskConfiguration implements JConfiguration<JDiskClient> {
    @NonNull
    private File bucketRoot;
    /**
     * 存储空间域名信息，用于生成访问地址，格式为：http[s]://www.example.com[/path]，需要自行处理该地址请求
     */
    @NonNull
    private Map<String, String> domainOfBucket = Collections.emptyMap();
}