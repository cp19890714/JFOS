/**
 * Copyright 2018-2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.oss;

import com.aliyun.oss.ClientBuilderConfiguration;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.aliyun.oss.common.auth.DefaultCredentials;
import com.jianggujin.fos.JClientWrapper;
import com.jianggujin.fos.JSecurityToken;

import lombok.Getter;

/**
 * @author jianggujin
 *
 */
@Getter
public class JOSSSecurityClientWrapper implements JClientWrapper<OSS> {
    private OSS client;
    private final String endPoint;
    private final JOSSSecurityTokenService securityTokenService;
    private final ClientBuilderConfiguration config;
    private volatile JSecurityToken securityToken;

    public JOSSSecurityClientWrapper(String endPoint, ClientBuilderConfiguration config,
            JOSSSecurityTokenService securityTokenService) {
        this.endPoint = endPoint;
        this.config = config;
        this.securityTokenService = securityTokenService;
    }

    @Override
    public OSS getClient() {
        if (client == null) {
            synchronized (this) {
                if (client == null) {
                    securityToken = securityTokenService.getSecurityToken();
                    this.client = new OSSClientBuilder()
                            .build(endPoint,
                                    new DefaultCredentialProvider(securityToken.getAccessKeyId(),
                                            securityToken.getSecretAccessKey(), securityToken.getSessionToken()),
                                    config);
                }
            }
            return client;
        }

        if (securityToken.isExpired()) {
            synchronized (this) {
                if (securityToken.isExpired()) {
                    securityToken = securityTokenService.getSecurityToken();
                    this.client.switchCredentials(new DefaultCredentials(securityToken.getAccessKeyId(),
                            securityToken.getSecretAccessKey(), securityToken.getSessionToken()));
                }
            }
        }
        return client;
    }

    @Override
    public void destory() {
        this.securityTokenService.destory();
        if (this.client != null) {
            this.client.shutdown();
        }
    }

}
