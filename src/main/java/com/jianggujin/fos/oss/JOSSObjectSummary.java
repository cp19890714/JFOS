/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.oss;

import java.util.Date;

import com.aliyun.oss.model.OSSObjectSummary;
import com.jianggujin.fos.JObjectSummary;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class JOSSObjectSummary extends OSSObjectSummary implements JObjectSummary {
    private OSSObjectSummary objectSummary;

    @Override
    public String getBucketName() {
        return objectSummary.getBucketName();
    }

    @Override
    public String getETag() {
        return objectSummary.getETag();
    }

    @Override
    public String getKey() {
        return objectSummary.getKey();
    }

    @Override
    public Date getLastModified() {
        return objectSummary.getLastModified();
    }

    @Override
    public long getSize() {
        return objectSummary.getSize();
    }
}
