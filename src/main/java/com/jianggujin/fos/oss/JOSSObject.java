/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.oss;

import java.io.IOException;
import java.io.InputStream;

import com.aliyun.oss.model.OSSObject;
import com.jianggujin.fos.JObject;
import com.jianggujin.fos.JObjectMetadata;

import lombok.Getter;

@Getter
public class JOSSObject implements JObject {
    private OSSObject object;
    private volatile JObjectMetadata objectMetadata;

    public JOSSObject(OSSObject object) {
        this.object = object;
    }

    @Override
    public void close() throws IOException {
        object.close();
    }

    @Override
    public InputStream getObjectContent() {
        return object.getObjectContent();
    }

    @Override
    public JObjectMetadata getObjectMetadata() {
        if (objectMetadata == null) {
            synchronized (this) {
                if (objectMetadata == null) {
                    objectMetadata = new JOSSObjectMetadata(object.getObjectMetadata());
                }
            }
        }
        return objectMetadata;
    }

    @Override
    public String getBucketName() {
        return object.getBucketName();
    }

    @Override
    public String getKey() {
        return object.getKey();
    }
}
