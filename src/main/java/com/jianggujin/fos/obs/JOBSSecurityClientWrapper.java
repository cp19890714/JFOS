/**
 * Copyright 2018-2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.obs;

import java.io.IOException;

import com.jianggujin.fos.JClientWrapper;
import com.jianggujin.fos.JSecurityToken;
import com.obs.services.ObsClient;
import com.obs.services.ObsConfiguration;

import lombok.Getter;

/**
 * @author jianggujin
 *
 */
@Getter
public class JOBSSecurityClientWrapper implements JClientWrapper<ObsClient> {
    private ObsClient client;
    private final JOBSSecurityTokenService securityTokenService;
    private final ObsConfiguration config;
    private volatile JSecurityToken securityToken;

    public JOBSSecurityClientWrapper(ObsConfiguration config, JOBSSecurityTokenService securityTokenService) {
        this.config = config;
        this.securityTokenService = securityTokenService;
    }

    @Override
    public ObsClient getClient() {
        if (client == null) {
            synchronized (this) {
                if (client == null) {
                    securityToken = securityTokenService.getSecurityToken();
                    this.client = new ObsClient(securityToken.getAccessKeyId(), securityToken.getSecretAccessKey(),
                            securityToken.getSessionToken(), config);
                }
            }
            return client;
        }
        if (securityToken.isExpired()) {
            synchronized (this) {
                if (securityToken.isExpired()) {
                    securityToken = securityTokenService.getSecurityToken();
                    this.client.refresh(securityToken.getAccessKeyId(), securityToken.getSecretAccessKey(),
                            securityToken.getSessionToken());
                }
            }
        }
        return client;
    }

    @Override
    public void destory() {
        this.securityTokenService.destory();
        if (client != null) {
            try {
                client.close();
            } catch (IOException e) {
            }
        }
    }

}
