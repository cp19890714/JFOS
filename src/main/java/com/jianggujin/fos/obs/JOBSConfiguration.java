/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.obs;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

import com.jianggujin.fos.JConfiguration;
import com.obs.services.HttpProxyConfiguration;
import com.obs.services.model.AuthTypeEnum;
import com.obs.services.model.HttpProtocolTypeEnum;

import lombok.Data;
import lombok.NonNull;
import okhttp3.Dispatcher;

@Data
public class JOBSConfiguration implements JConfiguration<JOBSClient> {

    @NonNull
    private String endPoint;
    @NonNull
    private String accessKey;
    @NonNull
    private String secretKey;

    private JOBSClientConfiguration clientConfiguration;
    private JOBSSessionTokenConfiguration sessionTokenConfiguration;

    /**
     * https://support.huaweicloud.com/api-iam/zh-cn_topic_0097949518.html
     * 
     * @author jianggujin
     *
     */
    @Data
    public static class JOBSSessionTokenConfiguration {
        private String securityToken;
        private String userName;
        private String password;
        private String domainName;
        private long durationSeconds = 900L;
        private boolean proxyIsable = false;
        private String proxyHostAddress;
        private int proxyPort = 8080;
        private String proxyUser;
        private String proxyPassword;
    }

    @Data
    public static class JOBSClientConfiguration {
        private Integer connectionTimeout;

        private Integer idleConnectionTime;

        private Integer maxIdleConnections;

        private Integer maxConnections;

        private Integer maxErrorRetry;

        private Integer socketTimeout;

        private Integer endpoIntegerHttpPort;

        private Integer endpoIntegerHttpsPort;

        private Boolean httpsOnly;

        private Boolean pathStyle;

        private HttpProxyConfiguration httpProxy;

        private Integer uploadStreamRetryBufferSize;

        private Boolean validateCertificate;

        private Boolean verifyResponseContentType;

        private Integer readBufferSize;

        private Integer writeBufferSize;

        private KeyManagerFactory keyManagerFactory;

        private TrustManagerFactory trustManagerFactory;

        private Boolean isStrictHostnameVerification;

        private AuthTypeEnum authType;

        private String signatString;
        private String defaultBucketLocation;
        private Integer bufferSize;
        private Integer socketWriteBufferSize;
        private Integer socketReadBufferSize;
        private Boolean isNio;
        private Boolean useReaper;
        private Boolean keepAlive;
        private Integer connectionRequestTimeout;
        private Boolean authTypeNegotiation;

        private Boolean cname;

        private String delimiter;

        private String sslProvider;

        private HttpProtocolTypeEnum httpProtocolType;

        private Dispatcher httpDispatcher;
    }

}