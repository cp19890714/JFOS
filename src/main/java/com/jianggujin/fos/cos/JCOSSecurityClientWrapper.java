/**
 * Copyright 2018-2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.cos;

import com.jianggujin.fos.JClientWrapper;
import com.jianggujin.fos.JSecurityToken;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicSessionCredentials;

import lombok.Getter;

/**
 * @author jianggujin
 *
 */
@Getter
public class JCOSSecurityClientWrapper implements JClientWrapper<COSClient> {
    private volatile COSClient client;
    private final JCOSSecurityTokenService securityTokenService;
    private final ClientConfig config;
    private volatile JSecurityToken securityToken;

    public JCOSSecurityClientWrapper(ClientConfig config, JCOSSecurityTokenService securityTokenService) {
        this.config = config;
        this.securityTokenService = securityTokenService;
    }

    @Override
    public COSClient getClient() {
        if (client == null) {
            synchronized (this) {
                if (client == null) {
                    securityToken = securityTokenService.getSecurityToken();
                    this.client = new COSClient(new BasicSessionCredentials(securityToken.getAccessKeyId(),
                            securityToken.getSecretAccessKey(), securityToken.getSessionToken()), config);
                }
            }
            return client;
        }
        if (securityToken.isExpired()) {
            synchronized (this) {
                if (securityToken.isExpired()) {
                    securityToken = securityTokenService.getSecurityToken();
                    this.client.setCOSCredentials(new BasicSessionCredentials(securityToken.getAccessKeyId(),
                            securityToken.getSecretAccessKey(), securityToken.getSessionToken()));
                }
            }
        }
        return client;
    }

    @Override
    public void destory() {
        this.securityTokenService.destory();
        if (this.client != null) {
            this.client.shutdown();
        }
    }

}
