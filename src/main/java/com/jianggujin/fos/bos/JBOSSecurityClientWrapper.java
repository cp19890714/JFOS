/**
 * Copyright 2018-2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.bos;

import com.baidubce.auth.DefaultBceSessionCredentials;
import com.baidubce.services.bos.BosClient;
import com.baidubce.services.bos.BosClientConfiguration;
import com.jianggujin.fos.JClientWrapper;
import com.jianggujin.fos.JSecurityToken;

import lombok.Getter;

/**
 * @author jianggujin
 *
 */
@Getter
public class JBOSSecurityClientWrapper implements JClientWrapper<BosClient> {
    private final BosClient client;
    private final JBOSSecurityTokenService securityTokenService;
    private final BosClientConfiguration config;
    private volatile JSecurityToken securityToken;

    public JBOSSecurityClientWrapper(BosClientConfiguration config, JBOSSecurityTokenService securityTokenService) {
        this.config = config;
        this.securityTokenService = securityTokenService;
        this.client = new BosClient(config);
    }

    @Override
    public BosClient getClient() {
        if (securityToken == null) {
            synchronized (this) {
                if (securityToken == null) {
                    securityToken = securityTokenService.getSecurityToken();
                    config.setCredentials(new DefaultBceSessionCredentials(securityToken.getAccessKeyId(),
                            securityToken.getSecretAccessKey(), securityToken.getSessionToken()));
                }
            }
            return client;
        }
        if (securityToken.isExpired()) {
            synchronized (this) {
                if (securityToken.isExpired()) {
                    securityToken = securityTokenService.getSecurityToken();
                    config.setCredentials(new DefaultBceSessionCredentials(securityToken.getAccessKeyId(),
                            securityToken.getSecretAccessKey(), securityToken.getSessionToken()));
                }
            }
        }
        return client;
    }

    @Override
    public void destory() {
        this.securityTokenService.destory();
        this.client.shutdown();
    }

}
