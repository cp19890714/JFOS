/**
 * Copyright 2019 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.fos.bos;

import java.util.ArrayList;
import java.util.List;

import com.baidubce.services.bos.model.BosObjectSummary;
import com.baidubce.services.bos.model.ListObjectsResponse;
import com.jianggujin.fos.JAbstractObjectListing;
import com.jianggujin.fos.JObjectListing;
import com.jianggujin.fos.JObjectSummary;

public class JBOSObjectListing extends JAbstractObjectListing implements JObjectListing {

    public JBOSObjectListing(ListObjectsResponse listing) {
        super(listing.getBucketName(), listing.getNextMarker(), listing.isTruncated(), listing.getPrefix(),
                listing.getMarker(), listing.getMaxKeys(), listing.getDelimiter());
        List<BosObjectSummary> summaries = listing.getContents();
        if (summaries.size() > 0) {
            List<JObjectSummary> objectSummaries = new ArrayList<JObjectSummary>(summaries.size());
            for (BosObjectSummary summary : summaries) {
                objectSummaries.add(new JBOSObjectSummary(summary));
            }
            setObjectSummaries(objectSummaries);
        }
    }
}
