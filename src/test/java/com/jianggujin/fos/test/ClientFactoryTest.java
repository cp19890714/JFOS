package com.jianggujin.fos.test;

import java.io.File;

import com.jianggujin.fos.JClientFactory;
import com.jianggujin.fos.JFOSException;
import com.jianggujin.fos.disk.JDiskClient;
import com.jianggujin.fos.disk.JDiskConfiguration;

public class ClientFactoryTest {

    @org.junit.Test
    public void test() throws JFOSException {
        File bucketRoot = new File("fos");
        JDiskConfiguration configuration = new JDiskConfiguration(bucketRoot);
        JDiskClient client = JClientFactory.newClient(configuration);
        client.destory();
    }
}
